<%-- 
    Document   : lista
    Created on : 24-04-2021, 12:21:13
    Author     : mlara
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="root.evaluacion2.entity.Matricula"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Matricula> matriculas = (List<Matricula>) request.getAttribute("matricula");
    Iterator<Matricula> itematriculas = matriculas.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form  name="form" action="MatriculaController" method="POST">


            <table border="1">
                <thead>
                <th>Rut</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Telefono</th>
                <th>Carrera</th>
                <th>Selección</th>
                <th> </th>

                </thead>
                <tbody>
                    <%while (itematriculas.hasNext()) {
                                Matricula cm = itematriculas.next();%>
                    <tr>
                        <td><%= cm.getRut()%></td>
                        <td><%= cm.getNombre()%></td>
                        <td><%= cm.getApellido()%></td>
                        <td><%= cm.getTelefono()%></td>
                        <td><%= cm.getCarrera()%></td>

                        <td> <input type="radio" name="seleccion" value="<%= cm.getRut()%>"> </td>

                    </tr>
                    <%}%>                
                </tbody>           
            </table>
            <button type="submit" name="accion" value="eliminar" class="btn btn-success">Eliminar Matricula</button>
            <button type="submit" name="accion" value="Consultar" class="btn btn-success">Editar Matricula</button>
            <button type="submit" name="accion" value="ingreso" class="btn btn-success">Ingresar Matricula</button> 
        </form>   
    </body>
</html>
