<%-- 
    Document   : index
    Created on : 24-04-2021, 11:27:50
    Author     : mlara
--%>

<%@page import="root.evaluacion2.entity.Matricula"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Matricula matri = (Matricula) request.getAttribute("Matri");
   
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Matriculas CIISA</title>
    </head>
    <body>
        <h1>Editar de Matriculas CIISA</h1>
         <form  name="form" action="MatriculaController" method="POST">


             <p>Ingrese Rut
                 <input type="text" name="rut" id="rut" value="<%= matri.getRut() %>"></p>  
             <p>Ingrese Nombre
                 <input type="text" name="nombre" id="nombre" value="<%= matri.getNombre() %>"></p>
             <p>Ingrese Apellido
                 <input type="text" name="apellido" id="apellido" value="<%= matri.getApellido() %>"></p>
             <p>Ingrese Telefono
                 <input type="text" name="telefono" id="telefono" value="<%= matri.getTelefono() %>"></p>
             <p>Ingrese Carrera
                 <input type="mail" name="carrera" id="carrera"value="<%= matri.getCarrera() %>"></p>

            <button type="submit" name="accion" value="editar" class="btn btn-success">Editar Ingreso</button>
         
        </form>
        
    </body>
</html>
