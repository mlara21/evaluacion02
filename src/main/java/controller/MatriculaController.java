/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.evaluacion2.dao.MatriculaJpaController;
import root.evaluacion2.dao.exceptions.NonexistentEntityException;
import root.evaluacion2.entity.Matricula;

/**
 *
 * @author mlara
 */
@WebServlet(name = "MatriculaController", urlPatterns = {"/MatriculaController"})
public class MatriculaController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MatriculaController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MatriculaController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

//DAO Para ingresas alumnos nuevos   
        String accion = request.getParameter("accion");
        if (accion.equals("ingresar")) {
            try {
                String rut = request.getParameter("rut");
                String nombre = request.getParameter("nombre");
                String apellido = request.getParameter("apellido");
                String telefono = request.getParameter("telefono");
                String carrera = request.getParameter("carrera");

                Matricula matricula = new Matricula();
                matricula.setRut(rut);
                matricula.setNombre(nombre);
                matricula.setApellido(apellido);
                matricula.setTelefono(telefono);
                matricula.setCarrera(carrera);

                MatriculaJpaController dao = new MatriculaJpaController();
                dao.create(matricula);
                List<Matricula> lista = dao.findMatriculaEntities();
                request.setAttribute("matricula", lista);
                request.getRequestDispatcher("lista.jsp").forward(request, response);

            } catch (Exception ex) {
                Logger.getLogger(MatriculaController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //VER LISTADO DE ALUMNOS
        if (accion.equals("verlistado")) {
            MatriculaJpaController dao = new MatriculaJpaController();
            List<Matricula> lista = dao.findMatriculaEntities();
            request.setAttribute("matricula", lista);
            request.getRequestDispatcher("lista.jsp").forward(request, response);

        }

        //DAO DE ELIMINACIÓN
        if (accion.equals("eliminar")) {

            try {
                String rut = request.getParameter("seleccion");
                MatriculaJpaController dao = new MatriculaJpaController();
                dao.destroy(rut);
                List<Matricula> lista = dao.findMatriculaEntities();
                request.setAttribute("matricula", lista);
                request.getRequestDispatcher("lista.jsp").forward(request, response);

            } catch (NonexistentEntityException ex) {
                Logger.getLogger(MatriculaController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (accion.equals("Consultar")) {
            String rut = request.getParameter("seleccion");
            MatriculaJpaController dao = new MatriculaJpaController();
            Matricula inscripcion = dao.findMatricula(rut);
            request.setAttribute("Matri", inscripcion);
            request.getRequestDispatcher("consultar.jsp").forward(request, response);

        }

        //DAO DE EDICIÓN
        if (accion.equals("editar")) {
            try {
                String rut = request.getParameter("rut");
                String nombre = request.getParameter("nombre");
                String apellido = request.getParameter("apellido");
                String telefono = request.getParameter("telefono");
                String carrera = request.getParameter("carrera");

                Matricula matricula = new Matricula();
                matricula.setRut(rut);
                matricula.setNombre(nombre);
                matricula.setApellido(apellido);
                matricula.setTelefono(telefono);
                matricula.setCarrera(carrera);

                MatriculaJpaController dao = new MatriculaJpaController();
                dao.edit(matricula);
                List<Matricula> lista = dao.findMatriculaEntities();
                request.setAttribute("matricula", lista);
                request.getRequestDispatcher("lista.jsp").forward(request, response);

            } catch (Exception ex) {
                Logger.getLogger(MatriculaController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        if (accion.equals("ingreso")) {

            request.getRequestDispatcher("index.jsp").forward(request, response);
        }

        processRequest(request, response);
    }


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
